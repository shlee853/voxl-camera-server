#!/bin/bash
################################################################################
# Copyright (c) 2020 ModalAI, Inc. All rights reserved.
#
# creates an ipk package from compiled ros nodes.
# be sure to build everything first with build.sh in docker
# run this on host pc
# UPDATE VERSION IN CONTROL FILE, NOT HERE!!!
#
################################################################################

set -e # exit on error to prevent bad ipk from being generated

################################################################################
# variables
################################################################################
VERSION=$(cat ipk/control/control | grep "Version" | cut -d' ' -f 2)
PACKAGE=$(cat ipk/control/control | grep "Package" | cut -d' ' -f 2)
IPK_NAME=${PACKAGE}_${VERSION}.ipk

DATA_DIR=ipk/data
CONTROL_DIR=ipk/control

echo ""
echo "Package Name   : " $PACKAGE
echo "Version Number : " $VERSION
echo "IPK     Name   : " $IPK_NAME

################################################################################
# start with a little cleanup to remove old files
################################################################################
sudo rm -rf ipk/control.tar.gz 2>/dev/null > /dev/null
sudo rm -rf $DATA_DIR/  2>/dev/null > /dev/null
sudo rm -rf ipk/data.tar.gz 2>/dev/null > /dev/null
sudo rm -rf $IPK_NAME 2>/dev/null > /dev/null

################################################################################
## copy useful files into data directory
################################################################################

# install as root so the package files will have the right permissions
cd build && sudo make DESTDIR=../ipk/data PREFIX=/usr install   && cd ../

sudo mkdir -p $DATA_DIR/etc/systemd/system/ 2>/dev/null > /dev/null
sudo cp service/*.service $DATA_DIR/etc/systemd/system/ 2>/dev/null > /dev/null
sudo mkdir -p $DATA_DIR/share/voxl-camera-server/ 2>/dev/null > /dev/null
sudo cp config/* $DATA_DIR/share/voxl-camera-server/ 2>/dev/null > /dev/null
sudo mkdir -p $DATA_DIR/usr/include 2>/dev/null > /dev/null
sudo cp include/common/voxl_camera_server.h $DATA_DIR/usr/include/ 2>/dev/null > /dev/null

################################################################################
# pack the control, data, and final ipk archives
################################################################################

cd $CONTROL_DIR/
tar --create --gzip -f ../control.tar.gz * 2>/dev/null > /dev/null
cd ../../

cd $DATA_DIR/
tar --create --gzip -f ../data.tar.gz * 2>/dev/null > /dev/null
cd ../../

ar -r $IPK_NAME ipk/control.tar.gz ipk/data.tar.gz ipk/debian-binary 2>/dev/null > /dev/null

echo ""
echo DONE