#!/bin/bash
################################################################################
# Copyright (c) 2020 ModalAI, Inc. All rights reserved.
################################################################################

sudo rm -rf build/ 2>/dev/null
sudo rm -rf ipk/data/ 2>/dev/null

rm -rf ipk/control.tar.gz 2>/dev/null
rm -rf ipk/data.tar.gz 2>/dev/null
rm -rf *.ipk 2>/dev/null
rm -rf .bash_history 2>/dev/null

echo ""
echo "Done cleaning"
echo ""