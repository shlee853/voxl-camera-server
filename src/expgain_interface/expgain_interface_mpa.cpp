/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include "common_defs.h"
#include "debug_log.h"
#include "expgain_interface_mpa.h"
#include "modal_pipe.h"

// -----------------------------------------------------------------------------------------------------------------------------
// Handles callbacks over pipe to set exposure and gain
// -----------------------------------------------------------------------------------------------------------------------------
void ExpGainMPA::CommandCallback(int ch, char* data, int bytes, void* context)
{
	// unused
	(void)ch;
#if 1
	if( bytes == sizeof(set_camera_exposure_gain_packet_t) )
	{
		set_camera_exposure_gain_packet_t* command = (set_camera_exposure_gain_packet_t*)data;
		if( command->magic_number == CAMERA_MAGIC_NUMBER )
		{
            uint64_t exposure_ms = command->exposure_ns/1000;
            int16_t gain = command->gain;
            ExpGainMPA* expGain = (ExpGainMPA*)context;

            VOXL_LOG_INFO( "Received exposure: %lld gain: %d\n", exposure_ms, gain);
				
			// Set the Gain and Exposure
			expGain->setExposureNs = command->exposure_ns;
			expGain->setGain = command->gain;
            expGain->receivedValidCommand = true;
		}
		else
		{
			VOXL_LOG_ERROR( "Invalid magic number: %d\n", command->magic_number );
		}

	}
	else
	{
        VOXL_LOG_ERROR( "Invalid packet side set_camera_exposure_gain_packet_t: %d should be: %d\n", 
            bytes, sizeof(set_camera_exposure_gain_packet_t));
	}
#endif
}

// -----------------------------------------------------------------------------------------------------------------------------
// Performs any one time initialization. This function should only be called once.
// -----------------------------------------------------------------------------------------------------------------------------
Status ExpGainMPA::Initialize(const ExpGainInterfaceData* pIntfData)
{
    Status status = S_OK;

    if (pIntfData != NULL)
    {
        VOXL_LOG_INFO( "Creating callback on channel: %d\n", pIntfData->channel );
        // Register the command callback for the camera channel
        if (pipe_server_set_control_cb(pIntfData->channel, CommandCallback, this) != 0)
        {
            VOXL_LOG_ERROR("ERROR: failed to create camera command callback\n");
            status = S_ERROR;
        }
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function performs any clean up tasks prior to object destruction
// -----------------------------------------------------------------------------------------------------------------------------
void ExpGainMPA::Destroy()
{
   
}

// -----------------------------------------------------------------------------------------------------------------------------
// Adds a frame to be used for calculating new values of exposure/gain
// -----------------------------------------------------------------------------------------------------------------------------
Status ExpGainMPA::GetNewExpGain(const ExpGainFrameData* pNewFrame, ExpGainResult* pNewExpGain)
{
    if( receivedValidCommand )
    {
        pNewExpGain->exposure = setExposureNs;
        pNewExpGain->gain     = setGain;
    }
    return S_OK;
}