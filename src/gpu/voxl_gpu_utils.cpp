/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#include <iostream>
#include <vector>
#include <string>
#include <tgmath.h>
#include "buffer_manager.h"
#include "voxl_gpu_utils.h"

Status Rotate(RotationAngle rotate, BufferInfo* pSrcBuffer, BufferInfo* pDstBuffer);

static const char rotateCLKernelAnyRotation_Work_In_Progress[] =
    "kernel void img_rotate(global uchar8* pDestBuffer,\n"
    "                       global uchar8* pSrcBuffer,\n"
    "                       int            width,\n"
    "                       int            height,\n"
    "                       int            stride,\n"
    "                       float          sinTheta,\n"
    "                       float          cosTheta)\n"
    "{\n"
    "    //Work-item gets its index within index space\n"
    "    const int ix = get_global_id(0);\n"
    "    const int iy = get_global_id(1);\n"
    "\n"
    "    ///<@todo There is a 32-bit extension instead of doing 16-bits\n"
    "    ///<@todo Partition the work across different threads\n"
    "    ///<@todo Optimize based on buffer access pattern\n"
    "\n"
    "    //Calculate location of data to move into (ix,iy)\n"
    "    int x0   = width >> 1;\n"
    "    int y0   = height >> 1;\n"
    "    int xOff = ix - x0;\n"
    "    int yOff = iy - y0;\n"
    "    int xpos = (int)(xOff*cosTheta + yOff*sinTheta + x0);\n"
    "    int ypos = (int)(yOff*cosTheta - xOff*sinTheta + y0);\n"
    "\n"
    "    if (((int)xpos>=0) && ((int)xpos< width) && ((int)ypos>=0) && ((int)ypos< height))\n"
    "    {\n"
    "        // Read (ix,iy) pSrcBuffer and store at (xpos,ypos) in pDestBuffer. Because we rotating about the origin\n"
    "        // there is no translation\n"
    "        uchar8 temp = pDestBuffer[iy*width+ix];\n"
    "\n"
    "        pDestBuffer[iy*width+ix]    = pSrcBuffer[ypos*width+xpos];\n"
    "        pSrcBuffer[ypos*width+xpos] = temp;\n"
    "    }\n"
    "}\n";

const char GpuRotation::RotateCLKernel[] =
    "kernel void img_rotate(global unsigned int* pDestBuffer,\n"
    "                       global unsigned int* pSrcBuffer,\n"
    "                       int         width,\n"
    "                       int         height,\n"
    "                       int         stride,\n"
    "                       int         isY,\n"
    "                       float       sinTheta,\n"
    "                       float       cosTheta)\n"
    "{\n"
    "    //Work-item gets its index within index space\n"
    "    int srciX  = get_global_id(0);\n"
    "    int srcrow = srciX % width;\n"
    "    int srccol = (srciX - (srcrow*width));\n"
    "    int dstrow = height - srcrow - 1;\n"
    "    int dstcol = srccol;\n"
    "    int srcix  = (srcrow*width)+srccol;\n"
    "    int dstix  = (dstrow*width)+dstcol;\n"
    "\n"
    "    ///<@todo Partition the work across different threads\n"
    "    ///<@todo Optimize based on buffer access pattern\n"
    "\n"
    "    if (isY == 1)\n"
    "    {\n"
    "        unsigned int value = 0;\n"
    "        value |= ((pSrcBuffer[srciX] & 0xff)       << 24);\n"
    "        value |= ((pSrcBuffer[srciX] & 0xff00)     << 8);\n"
    "        value |= ((pSrcBuffer[srciX] & 0xff0000)   >> 8);\n"
    "        value |= ((pSrcBuffer[srciX] & 0xff000000) >> 24);\n"
    "        pDestBuffer[((width*height)/4)-1-srciX] = value;\n"
    "    }\n"
    "    else // doing UV\n"
    "    {\n"
    "        // Read (ix,iy) pSrcBuffer and store at (xpos,ypos) in pDestBuffer. Because we rotating about the origin\n"
    "        // there is no translation\n"
    "        unsigned int value = pSrcBuffer[srciX];\n"
    "        value <<= 16;\n"
    "        value |= (pSrcBuffer[srciX] >> 16);\n"
    "        pDestBuffer[((width*height)/4)-1-srciX] = value;\n"
    "    }\n"
    "}\n";

const char GpuStereoSplit::SplitCLKernelRaw10[] =
    "kernel void img_split(global unsigned int* pDestLeftBuffer,\n"
    "                      global unsigned int* pDestRightBuffer,\n"
    "                      global unsigned int* pSrcBuffer,\n"
    "                      int width,\n"
    "                      int height,\n"
    "                      int stride)\n"
    "{\n"
    "    //Work-item gets its index within index space\n"
    "    int row                 = get_global_id(0);\n"
    "    ///<@todo Assumes RAW10 format\n"
    "    ///<@todo Assumes unsigned int is 4 bytes\n"
    "    unsigned int  rowBytes  = (unsigned int)(width+(width/4));\n"
    "    unsigned int* pSrcLAddr = (pSrcBuffer + ((row*stride)/4));\n"
    "    unsigned int* pSrcRAddr = (pSrcLAddr  + ((rowBytes/2)/4));\n"
    "    unsigned int* pDstLAddr = (pDestLeftBuffer  + (((row*stride)/2)/4));\n"
    "    unsigned int* pDstRAddr = (pDestRightBuffer + (((row*stride)/2)/4));\n"
    "\n"
    "    ///<@todo Optimize based on buffer access pattern\n"
    "\n"
    "    ///<@todo Assumes unsigned int is 4 bytes\n"
    "    for (int i=0; i < ((rowBytes)/4)/2; i++)\n"
    "    {\n"
    "        pDstLAddr[i] = pSrcLAddr[i];\n"
    "        pDstRAddr[i] = pSrcRAddr[i];\n"
    "    }\n"
    "}\n";

const char GpuStereoSplit::SplitCLKernelRaw8[] =
    "kernel void img_split(global unsigned int* pDestLeftBuffer,\n"
    "                      global unsigned int* pDestRightBuffer,\n"
    "                      global unsigned int* pSrcBuffer,\n"
    "                      int width,\n"
    "                      int height,\n"
    "                      int stride)\n"
    "{\n"
    "    //Work-item gets its index within index space\n"
    "    int row                 = get_global_id(0);\n"
    "    ///<@todo Assumes Raw8 format\n"
    "    ///<@todo Assumes unsigned int is 4 bytes\n"
    "    unsigned int  rowBytes  = (unsigned int)(width);\n"
    "    unsigned int* pSrcLAddr = (pSrcBuffer + ((row*stride)/4));\n"
    "    unsigned int* pSrcRAddr = (pSrcLAddr  + ((width/2)/4));\n"
    "    unsigned int* pDstLAddr = (pDestLeftBuffer  + (((row*stride)/2)/4));\n"
    "    unsigned int* pDstRAddr = (pDestRightBuffer + (((row*stride)/2)/4));\n"
    "\n"
    "    ///<@todo Optimize based on buffer access pattern\n"
    "\n"
    "    ///<@todo Assumes unsigned int is 4 bytes\n"
    "    for (int i=0; i < (rowBytes/2)/4; i++)\n"
    "    {\n"
    "        pDstLAddr[i] = pSrcLAddr[i];\n"
    "        pDstRAddr[i] = pSrcRAddr[i];\n"
    "    }\n"
    "}\n";

// -----------------------------------------------------------------------------------------------------------------------------
// Create an instance of the requested object
// -----------------------------------------------------------------------------------------------------------------------------
ImageRotation* ImageRotation::Create(RotationCreateData* pCreateData)
{
    ImageRotation* pRotation = NULL;

    switch (pCreateData->api)
    {
        case ROTATION_GPU_OPENCL:
            pRotation = new GpuRotation;
            pRotation->Initialize(pCreateData);
            break;
        default:
            VOXL_LOG_FATAL("------voxl-camera-server FATAL: Cannot create rotation object\n");
            break;
    }

    return pRotation;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Perform any one time initialization
// -----------------------------------------------------------------------------------------------------------------------------
Status GpuRotation::Initialize(RotationCreateData* pCreateData)
{
    Status status = S_OK;

    cl::Platform::get(&m_platform);

	if (m_platform.empty())
    {
	    VOXL_LOG_FATAL("------voxl-camera-server FATAL: OpenCL platforms not found\n");
        status = S_ERROR;
	}

    if (status == S_OK)
    {
        m_platform[0].getDevices(CL_DEVICE_TYPE_GPU, &m_devices);
        m_pContext = new cl::Context(m_devices[0]);

        // std::cout << m_devices[0].getInfo<CL_DEVICE_NAME>() << std::endl;
        // std::cout << m_devices[0].getInfo<CL_DRIVER_VERSION>() << std::endl;

        // Create command queue.
        m_pQueue = new cl::CommandQueue(*m_pContext, m_devices[0]);

        // Compile OpenCL program
        m_pProgram = new cl::Program(*m_pContext,
                                     cl::Program::Sources(1, std::make_pair(RotateCLKernel, strlen(RotateCLKernel))));

        try
        {
            m_pProgram->build(m_devices);
        }
        catch (const cl::Error&)
        {
            std::cerr
            << "OpenCL compilation error" << std::endl
            << m_pProgram->getBuildInfo<CL_PROGRAM_BUILD_LOG>(m_devices[0])
            << std::endl;

            return S_ERROR;
        }

        m_pKernel = new cl::Kernel(*m_pProgram, "img_rotate");

        m_rotationAngle = pCreateData->angle;
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Function that performs image rotation
// -----------------------------------------------------------------------------------------------------------------------------
Status GpuRotation::Rotate(RotationExecData* pExecData)
{
    Status status = S_OK;

    BufferInfo* pSrcBuffer = pExecData->pSrcBuffer;
    BufferInfo* pDstBuffer = pExecData->pDstBuffer;
    ///<@todo Assumes src and dst have the same dimensions
    int width  = pDstBuffer->width;
    int height = pDstBuffer->height;
    int stride = pDstBuffer->stride;

    ///<@todo Only supports NV12 for now
    uint32_t yChannelBytes  = height * stride;
    uint32_t uvChannelBytes = (yChannelBytes >> 1);

    // Allocate device buffers and transfer input data to device.
    cl::Buffer clBufferSrcY(*m_pContext, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, yChannelBytes, pSrcBuffer->vaddr);
    cl::Buffer clBufferDstY(*m_pContext, CL_MEM_READ_WRITE , yChannelBytes);
    ///<@todo Only supports NV12 for now
    cl::Buffer clBufferSrcUV(*m_pContext, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, uvChannelBytes, pSrcBuffer->craddr);
    cl::Buffer clBufferDstUV(*m_pContext, CL_MEM_READ_WRITE , uvChannelBytes);

    // Set kernel parameters (dest, src, width, height, sin(theta), cos(theta))
    m_pKernel->setArg(0, clBufferDstY);
    m_pKernel->setArg(1, clBufferSrcY);
    m_pKernel->setArg(2, width);
    m_pKernel->setArg(3, height);
    m_pKernel->setArg(4, stride);
    m_pKernel->setArg(5, 1); // 1 indicates Y channel
    m_pKernel->setArg(6, (float)sin(m_rotationAngle));
    m_pKernel->setArg(7, (float)cos(m_rotationAngle));

    // Launch kernel on the compute device
    ///<@todo Do the global, local workgroup size. In each one of the instance of the kernel copy one entire row
    m_pQueue->enqueueNDRangeKernel(*m_pKernel, cl::NullRange, yChannelBytes/4, cl::NullRange);
    // Get result back to host
    m_pQueue->enqueueReadBuffer(clBufferDstY, CL_TRUE, 0, yChannelBytes, pSrcBuffer->vaddr);

    // Repeat the same for the Y channel
    m_pKernel->setArg(0, clBufferDstUV);
    m_pKernel->setArg(1, clBufferSrcUV);
    m_pKernel->setArg(2, width);
    m_pKernel->setArg(3, height/2);
    m_pKernel->setArg(4, stride);
    m_pKernel->setArg(5, 0); // 0 indicates UV channel

    m_pQueue->enqueueNDRangeKernel(*m_pKernel, cl::NullRange, uvChannelBytes/4, cl::NullRange);
    // Get result back to host
    m_pQueue->enqueueReadBuffer(clBufferDstUV, CL_TRUE, 0, uvChannelBytes, pSrcBuffer->craddr);

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Create an instance of the requested object
// -----------------------------------------------------------------------------------------------------------------------------
StereoImageSplit* StereoImageSplit::Create(StereoSplitCreateData* pCreateData)
{
    StereoImageSplit* pSplit = NULL;

    switch (pCreateData->api)
    {
        case SPLIT_GPU_OPENCL:
            pSplit = new GpuStereoSplit;
            pSplit->Initialize(pCreateData);
            break;
        default:
            VOXL_LOG_FATAL("------voxl-camera-server FATAL: Cannot create stereo-split object\n");
            break;
    }

    return pSplit;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Perform any one time initialization
// -----------------------------------------------------------------------------------------------------------------------------
Status GpuStereoSplit::Initialize(StereoSplitCreateData* pCreateData)
{
    Status status = S_OK;

    cl::Platform::get(&m_platform);

	if (m_platform.empty())
    {
	    VOXL_LOG_FATAL("------voxl-camera-server FATAL: OpenCL platforms not found\n");
        status = S_ERROR;
	}

    if (status == S_OK)
    {
        ///<@todo Start using this
        m_previewFormat = pCreateData->format;

        m_platform[0].getDevices(CL_DEVICE_TYPE_GPU, &m_devices);
        m_pContext = new cl::Context(m_devices[0]);

        // std::cout << m_devices[0].getInfo<CL_DEVICE_NAME>() << std::endl;
        // std::cout << m_devices[0].getInfo<CL_DRIVER_VERSION>() << std::endl;

        // Create command queue.
        m_pQueue = new cl::CommandQueue(*m_pContext, m_devices[0]);

        // Compile OpenCL program
        m_pProgram = new cl::Program(*m_pContext,
                                    cl::Program::Sources(1, std::make_pair(SplitCLKernelRaw8, strlen(SplitCLKernelRaw8))));

        try
        {
            m_pProgram->build(m_devices);
        }
        catch (const cl::Error&)
        {
            std::cerr
            << "OpenCL compilation error" << std::endl
            << m_pProgram->getBuildInfo<CL_PROGRAM_BUILD_LOG>(m_devices[0])
            << std::endl;

            return S_ERROR;
        }

        m_pKernel = new cl::Kernel(*m_pProgram, "img_split");
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Function that performs image split (into left and right images)
// -----------------------------------------------------------------------------------------------------------------------------
Status GpuStereoSplit::Split(StereoSplitExecData* pExecData)
{
    Status status = S_OK;

    unsigned int* pSrcBuffer  = pExecData->pSrcBuffer;
    unsigned int* pDstLBuffer = pExecData->pDstLBuffer;
    unsigned int* pDstRBuffer = pExecData->pDstRBuffer;
    ///<@todo Assumes raw8
    unsigned int  leftrightSize = (pExecData->width/2) * pExecData->height;
    ///<@todo Assumes src and dst have the same dimensions
    int width  = pExecData->width;
    int height = pExecData->height;
    int stride = pExecData->stride;

    // Allocate device buffers and transfer input data to device.
    cl::Buffer clBufferSrc(*m_pContext, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, pExecData->sizeBytes, pSrcBuffer);
    cl::Buffer clBufferDstL(*m_pContext, CL_MEM_READ_WRITE, leftrightSize);
    cl::Buffer clBufferDstR(*m_pContext, CL_MEM_READ_WRITE, leftrightSize);

    // Set kernel parameters (dest, src, width, height, sin(theta), cos(theta))
    m_pKernel->setArg(0, clBufferDstL);
    m_pKernel->setArg(1, clBufferDstR);
    m_pKernel->setArg(2, clBufferSrc);
    m_pKernel->setArg(3, width);
    m_pKernel->setArg(4, height);
    m_pKernel->setArg(5, stride);

    // Launch kernel on the compute device
    ///<@todo Do the global, local workgroup size. In each one of the instance of the kernel copy one entire row
    m_pQueue->enqueueNDRangeKernel(*m_pKernel, cl::NullRange, height, cl::NullRange);
    // Get result back to host
    m_pQueue->enqueueReadBuffer(clBufferDstL, CL_TRUE, 0, leftrightSize, pDstLBuffer);
    m_pQueue->enqueueReadBuffer(clBufferDstR, CL_TRUE, 0, leftrightSize, pDstRBuffer);

    return status;
}