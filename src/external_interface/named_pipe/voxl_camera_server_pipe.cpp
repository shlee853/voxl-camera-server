// /*******************************************************************************
//  * Copyright 2020 ModalAI Inc.
//  *
//  * Redistribution and use in source and binary forms, with or without
//  * modification, are permitted provided that the following conditions are met:
//  *
//  * 1. Redistributions of source code must retain the above copyright notice,
//  *    this list of conditions and the following disclaimer.
//  *
//  * 2. Redistributions in binary form must reproduce the above copyright notice,
//  *    this list of conditions and the following disclaimer in the documentation
//  *    and/or other materials provided with the distribution.
//  *
//  * 3. Neither the name of the copyright holder nor the names of its contributors
//  *    may be used to endorse or promote products derived from this software
//  *    without specific prior written permission.
//  *
//  * 4. The Software is used solely in conjunction with devices provided by
//  *    ModalAI Inc.
//  *
//  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//  * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
//  * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
//  * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
//  * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
//  * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
//  * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
//  * POSSIBILITY OF SUCH DAMAGE.
//  ******************************************************************************/

#include <string.h>
#include "common_defs.h"
#include "debug_log.h"
#include "voxl_camera_server.h"
#include "voxl_camera_server_pipe.h"

#define camMultiplier(x) (x==CAMTYPE_HIRES?32:16)

// Initialize static pointer variable to NULL
NamedPipe* NamedPipe::m_spNamedPipe = NULL;

//------------------------------------------------------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------------------------------------------------------
NamedPipe::NamedPipe()
{
}

//------------------------------------------------------------------------------------------------------------------------------
// Create a singleton instance of this class
//------------------------------------------------------------------------------------------------------------------------------
ExternalInterface* NamedPipe::Create()
{
    if (m_spNamedPipe == NULL)
    {
        m_spNamedPipe = new NamedPipe;
    }

    return m_spNamedPipe;
}

//------------------------------------------------------------------------------------------------------------------------------
// Destroy the instance
//------------------------------------------------------------------------------------------------------------------------------
void NamedPipe::Destroy()
{
    if (m_spNamedPipe != NULL)
    {
        pipe_server_close_all();
        delete m_spNamedPipe;
        m_spNamedPipe = NULL;
    }
}

//------------------------------------------------------------------------------------------------------------------------------
// libmodal_pipe calls into this callback when a client subscribes to the channel. This function triggers the start of the
// camera to which the client subscibes to. If the camera has already been started because of an earlier client, nothing is
// done.
//------------------------------------------------------------------------------------------------------------------------------
void NamedPipe::RequestPipeHandler(int channel, char* pClientName, int bytes, int clientid, void* pContext)
{
    if (m_spNamedPipe != NULL)
    {
        // Start the requested camera if it hasn't already been started
        m_spNamedPipe->HandleClientRequest(channel, pClientName, bytes, clientid);
    }
}

//------------------------------------------------------------------------------------------------------------------------------
// libmodal_pipe calls into this callback when a client disconnects from the channel.
//------------------------------------------------------------------------------------------------------------------------------
void NamedPipe::DisconnectPipeHandler(int ch, int client_id, char* name, void* context)
{
    if (m_spNamedPipe != NULL)
    {
        m_spNamedPipe->HandleClientDisconnect(ch);
    }
}

//------------------------------------------------------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------------------------------------------------------
void NamedPipe::HandleClientRequest(int channel, char* pClientName, int bytes, int clientid)
{

    m_interfaceData.ClientJoined(channel);
}

//------------------------------------------------------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------------------------------------------------------
void NamedPipe::HandleClientDisconnect(int channel){
    m_interfaceData.ClientLeft(channel);
}

//------------------------------------------------------------------------------------------------------------------------------
// Do any one time initialization in this function
//------------------------------------------------------------------------------------------------------------------------------
Status NamedPipe::Initialize(int                    numCameras,         ///< Number of cameras
                             const PerCameraInfo*   pPerCameraInfo,     ///< Per camera info
                             ExternalInterfaceData* pExtIntfData)       ///< Data provided by server core to external interface
{
    Status status = S_OK;

    int numChannels = 0;
    for (int i = 0; i < numCameras; i++)
    {
        if (pPerCameraInfo[i].isEnabled)
        {


            char pipeName[256] = MODAL_PIPE_DEFAULT_BASE_DIR;
            strcat(pipeName, pPerCameraInfo[i].name);

            switch (pPerCameraInfo[i].type)
            {
                case CAMTYPE_HIRES:
                    strcat(pipeName, "_preview");
                case CAMTYPE_TRACKING:
                case CAMTYPE_STEREO:
                    strcat(pipeName, "/");
                    // 0 means success
                    VOXL_LOG_INFO( "Creating pipe: %s channel: %d\n", pipeName, numChannels);
                    if (0 == pipe_server_init_channel(numChannels,
                                                      pipeName,
                                                      ControlPipeEnabled))
                    {
                        pipe_server_set_default_pipe_size(numChannels,
                                                 camMultiplier(pPerCameraInfo[i].type)*1024*1024);
                        pipe_server_set_request_cb(numChannels, RequestPipeHandler, NULL);
                        pipe_server_set_disconnect_cb(numChannels, DisconnectPipeHandler, NULL);
                    } else {
                        status = S_ERROR;
                    }
                    numChannels++;

                    break;

                case CAMTYPE_TOF:

                    char tofName[256];
                    for(int j = 0; j < NUM_TOF_CHANNELS; j++){
                        strcpy(tofName, pipeName);
                        strcat(tofName, tofChannelNames[j]);
                        if (0 == pipe_server_init_channel(numChannels,
                                                          tofName,
                                                          ControlPipeDisabled | SERVER_FLAG_EN_INFO_PIPE))
                        {
                            VOXL_LOG_INFO( "Creating pipe: %s channel: %d\n", tofName, numChannels);
                            pipe_server_set_default_pipe_size(numChannels,TOF_RECOMMENDED_PIPE_SIZE);
                            pipe_server_set_request_cb(numChannels, RequestPipeHandler, NULL);
                            pipe_server_set_disconnect_cb(numChannels, DisconnectPipeHandler, NULL);
                        } else {
                            status = S_ERROR;
                        }
                        numChannels++;
                    }

                    break;

                default:
                    VOXL_LOG_WARNING("------voxl-camera-server WARNING: Bad camera type: %d\n", pPerCameraInfo[i].type);
                    break;
            }
        }
    }

    if (status == S_OK)
    {
        m_interfaceData = *pExtIntfData;
    }

    return status;
}

//------------------------------------------------------------------------------------------------------------------------------
// Broadcast the camera frame to all clients
//------------------------------------------------------------------------------------------------------------------------------
Status NamedPipe::BroadcastFrame(int channel,
                                 camera_image_metadata_t meta,
                                 char* pFrameData)
{
    pipe_server_send_camera_frame_to_channel(channel, meta, pFrameData);

    return S_OK;
}
//------------------------------------------------------------------------------------------------------------------------------
// Broadcast the point cloud to all clients
//------------------------------------------------------------------------------------------------------------------------------
Status NamedPipe::BroadcastPointCloud(int channel,
                                      point_cloud_metadata_t meta,
                                      float* pFrameData)
{
    pipe_server_send_point_cloud_to_channel(channel, meta, pFrameData);

    return S_OK;
}
//------------------------------------------------------------------------------------------------------------------------------
// Broadcast the point cloud to all clients
//------------------------------------------------------------------------------------------------------------------------------
Status NamedPipe::BroadcastGeneric(int channel,
                                   const char *data,
                                   int bytes)
{
    pipe_server_send_to_channel(channel, (char *)data, bytes);

    return S_OK;
}