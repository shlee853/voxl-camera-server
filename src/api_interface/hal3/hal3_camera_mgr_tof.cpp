/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <sys/resource.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <errno.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <chrono>
#include <cutils/properties.h>
#include <modal_json.h>
#include <modal_pipe_interfaces.h>
#include "buffer_manager.h"
#include "hal3_camera.h"
#include <royale/DepthData.hpp>
#include <TOFInterface.h>
#include "common_tools.h"



// Main thread functions for request and result processing
void* ThreadPostProcessResultTof(void* data);
extern void* ThreadIssueCaptureRequests(void* data);

extern int g_outputChannels[CAMTYPE_MAX_TYPES];

// -----------------------------------------------------------------------------------------------------------------------------
// Filled in when the camera module sends result image buffers to us. This gets passed to the capture result handling threads's
// message queue
// -----------------------------------------------------------------------------------------------------------------------------
struct CaptureResultFrameData
{
    // Either preview or video or both may be valid
    BufferInfo* pPreviewBufferInfo;     ///< Preview buffer information
    BufferInfo* pVideoBufferInfo;       ///< Video buffer information
    int64_t     timestampNsecs;         ///< Timestamp of the buffer(s) in nano secs
    int         frameNumber;            ///< Frame number associated with the image buffers
};

// -----------------------------------------------------------------------------------------------------------------------------
// Constructor
// -----------------------------------------------------------------------------------------------------------------------------
PerCameraMgrTof::PerCameraMgrTof()
{

    for (uint32_t i = 0; i < StreamTypeMax; i++)
    {
        m_pBufferManager[i] = NULL;
    }

    m_requestThread.pCameraMgr = this;
    m_requestThread.stop       = false;
    m_requestThread.pPrivate   = NULL;
    m_requestThread.msgQueue.clear();

    m_resultThread.pCameraMgr = this;
    m_resultThread.stop       = false;
    m_resultThread.pPrivate   = NULL;
    m_resultThread.msgQueue.clear();
    m_resultThread.lastResultFrameNumber = -1;

    m_pTOFInterface = NULL;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Performs any one time initialization. This function should only be called once.
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgrTof::Initialize(PerCameraInitData* pPerCameraInitData)
{
    int status = 0;

    const PerCameraInfo* pCameraInfo = pPerCameraInitData->pPerCameraInfo;
    memcpy(&m_cameraConfigInfo, pCameraInfo, sizeof(PerCameraInfo));

    m_pCameraModule       = pPerCameraInitData->pCameraModule;
    m_pHalCameraInfo      = pPerCameraInitData->pHalCameraInfo;
    m_cameraId            = pPerCameraInitData->cameraid;
    m_pApiInterfaceData   = pPerCameraInitData->pApiInterface;
    printf("%s\n", GetTypeString(GetCameraType()));
    m_outputChannel       = g_outputChannels[GetCameraType()];

    m_cameraCallbacks.cameraCallbacks = {CameraModuleCaptureResult, CameraModuleNotify};
    m_cameraCallbacks.pPrivate        = this;

    if ((pCameraInfo->modeInfo[CAMMODE_PREVIEW].format == PREVIEW_FMT_NV21) ||
        (pCameraInfo->modeInfo[CAMMODE_PREVIEW].format == PREVIEW_FMT_NV12))
    {
        m_previewHalFmt = HAL_PIXEL_FORMAT_YCbCr_420_888;
    }
    else if ((pCameraInfo->modeInfo[CAMMODE_PREVIEW].format == PREVIEW_FMT_RAW10) ||
             (pCameraInfo->modeInfo[CAMMODE_PREVIEW].format == PREVIEW_FMT_RAW8))
    {
        m_previewHalFmt = HAL_PIXEL_FORMAT_RAW10;
    }
    else if (pCameraInfo->modeInfo[CAMMODE_PREVIEW].format == PREVIEW_FMT_BLOB)
    {
        m_previewHalFmt = HAL_PIXEL_FORMAT_BLOB;
    }

    // Check if the stream configuration is supported by the camera or not. If cameraid doesnt support the stream configuration
    // we just exit. The stream configuration is checked into the static metadata associated with every camera.
    if (IsStreamConfigSupported(GetPreviewWidth(), GetPreviewHeight(), m_previewHalFmt) == false)
    {
        status = -EINVAL;
    }

    char cameraName[20] = {0};
    sprintf(cameraName, "%d", m_cameraId);

    if (status == 0)
    {
        status = m_pCameraModule->common.methods->open(&m_pCameraModule->common, cameraName, (hw_device_t**)(&m_pDevice));

        if (status != 0)
        {
            VOXL_LOG_ERROR("------voxl-camera-server ERROR: Open camera %s failed!\n", cameraName);
        }
    }

    if (status == 0)
    {
        status = m_pDevice->ops->initialize(m_pDevice, (camera3_callback_ops*)&m_cameraCallbacks);

        if (status != 0)
        {
            VOXL_LOG_ERROR("------voxl-camera-server ERROR: Initialize camera %s failed!\n", cameraName);
        }
    }

    if (status == 0)
    {
        // This calls into the camera module and checks if it supports the stream configuration. If it doesnt then we have to
        // bail out.
        status = ConfigureStreams();
    }

    // Since ConfigureStreams is successful lets allocate the buffer memory since we are definitely going to start processing
    // camera frames now
    if (status == 0)
    {
        status = AllocateStreamBuffers();
    }

    if (status == 0)
    {
        // This is the default metadata i.e. camera settings per request. The camera module passes us the best set of baseline
        // settings. We can modify any setting, for any frame or for every frame, as we see fit.
        ConstructDefaultRequestSettings();
    }

    if (status == 0)
    {
        m_pTOFInterface = TOFCreateInterface();

        if (m_pTOFInterface != NULL)
        {
            printf("SUCCESS: TOF interface created!\n");

            modalai::RoyaleListenerType dataType;

            dataType = modalai::RoyaleListenerType::LISTENER_DEPTH_DATA;

            TOFInitializationData initializationData = { 0 };

            initializationData.pTOFInterface = m_pTOFInterface;
            initializationData.pDataTypes    = &dataType;
            initializationData.numDataTypes  = 1;
            initializationData.pListener     = this;
            initializationData.frameRate     = pCameraInfo->fps;
            switch(pCameraInfo->tofMode){

                case 5: 
                    initializationData.range = RoyaleDistanceRange::SHORT_RANGE;
                    break;
                case 9: 
                    initializationData.range = RoyaleDistanceRange::LONG_RANGE;
                    break;
                /*case 11: Not enabled in current release
                    initializationData.range = RoyaleDistanceRange::EXTRA_LONG_RANGE;
                    break;*/
                default:
                    VOXL_LOG_ERROR("------ voxl-camera-server ERROR: TOF mode :%d not supported\n", pCameraInfo->tofMode);
                    VOXL_LOG_ERROR("\t\tSupported modes are: 5 and 9\n");

            }

            status = TOFInitialize(&initializationData);
            if(status == 0){
                printf("SUCCESS: TOF interface initialized!\n");
            } else {
                printf("ERROR: TOF interface not initialized!\n");
            }
        }
        else
        {
            printf("ERROR: Cannot create TOF interface!\n");
            status = -EINVAL;
        }

        if (status == 0)
        {
            printf("Libcamera sending RAW16 TOF data. App calling the PMD libs to postprocess the RAW16 data\n");
        }
        else
        {
            printf("ERROR: Cannot initialize TOF interface to PMD libs!\n");
        }
        
    }

    if (status == 0)
    {
        LoadLensParamsFromFile();
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Construct default camera settings that will be passed to the camera module to be used for capturing the frames
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgrTof::ConstructDefaultRequestSettings()
{
    int fpsRange[] = {m_cameraConfigInfo.fps, m_cameraConfigInfo.fps};
    camera3_request_template_t type = CAMERA3_TEMPLATE_PREVIEW;

    // Get the default baseline settings
    camera_metadata_t* pDefaultMetadata = (camera_metadata_t *)m_pDevice->ops->construct_default_request_settings(m_pDevice,
                                                                                                                  type);

    // Modify all the settings that we want to
    m_requestMetadata = clone_camera_metadata(pDefaultMetadata);
    m_requestMetadata.update(ANDROID_CONTROL_AE_TARGET_FPS_RANGE, &fpsRange[0], 2);

    uint8_t antibanding = ANDROID_CONTROL_AE_ANTIBANDING_MODE_AUTO;
    m_requestMetadata.update(ANDROID_CONTROL_AE_ANTIBANDING_MODE,&(antibanding),sizeof(antibanding));

    uint8_t afmode = ANDROID_CONTROL_AF_MODE_CONTINUOUS_VIDEO;
    m_requestMetadata.update(ANDROID_CONTROL_AF_MODE, &(afmode), 1);

    uint8_t reqFaceDetectMode =  (uint8_t)ANDROID_STATISTICS_FACE_DETECT_MODE_OFF;
    m_requestMetadata.update(ANDROID_STATISTICS_FACE_DETECT_MODE, &reqFaceDetectMode, 1);

    uint8_t aeMode         = 0;
    // These are some (psuedo random) initial default values
    int     gainTarget     = 200;
    int64_t exposureTarget = 2259763; // nsecs exposure time
    m_requestMetadata.update(ANDROID_CONTROL_AE_MODE, &aeMode, 1);
    m_requestMetadata.update(ANDROID_SENSOR_SENSITIVITY, &gainTarget, 1);
    m_requestMetadata.update(ANDROID_SENSOR_EXPOSURE_TIME, &exposureTarget, 1);  // Exposure time in nsecs

    uint8_t data = (uint8_t) modalai::RoyaleListenerType::LISTENER_DEPTH_DATA;

    m_requestMetadata.update(ANDROID_TOF_DATA_OUTPUT, &data, 1);
}
// -----------------------------------------------------------------------------------------------------------------------------
// This function opens the camera and starts sending the capture requests
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgrTof::Start()
{
    int status = 0;

    m_requestThread.pDevice = m_pDevice;
    m_resultThread.pDevice  = m_pDevice;

    pthread_condattr_t attr;
    pthread_condattr_init(&attr);
    pthread_condattr_setclock(&attr, CLOCK_MONOTONIC);
    pthread_mutex_init(&m_requestThread.mutex, NULL);
    pthread_mutex_init(&m_resultThread.mutex, NULL);
    pthread_cond_init(&m_requestThread.cond, &attr);
    pthread_cond_init(&m_resultThread.cond, &attr);
    pthread_condattr_destroy(&attr);

    // Start the thread that will process the camera capture result. This thread wont exit till it consumes all expected
    // output buffers from the camera module or it encounters a fatal error
    pthread_attr_t resultAttr;
    pthread_attr_init(&resultAttr);
    pthread_attr_setdetachstate(&resultAttr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&(m_resultThread.thread), &resultAttr, ThreadPostProcessResultTof, &m_resultThread);
    pthread_attr_destroy(&resultAttr);

    // Start the thread that will send the camera capture request. This thread wont stop issuing requests to the camera
    // module until we terminate the program with Ctrl+C or it encounters a fatal error
    pthread_attr_t requestAttr;
    pthread_attr_init(&requestAttr);
    pthread_attr_setdetachstate(&requestAttr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&(m_requestThread.thread), &requestAttr, ThreadIssueCaptureRequests, &m_requestThread);
    pthread_attr_destroy(&requestAttr);

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function stops the camera and does all necessary clean up
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgrTof::Stop()
{
    ///< @todo Need to wait for all Royale postprocessed frames to return. We may have submitted frames to Royale for processing
    ///        that we may not have received back. We need to bail out elegantly waiting for Royale to finish.
    //Stop has already been called
    if(!m_requestThread.EStop){
        // Not an emergency stop, wait to recieve last frame
        // The result thread will stop when the result of the last frame is received
        m_requestThread.stop = true;
    }
    pthread_join(m_requestThread.thread, NULL);
    pthread_cond_signal(&m_requestThread.cond);
    pthread_mutex_unlock(&m_requestThread.mutex);
    pthread_mutex_destroy(&m_requestThread.mutex);
    pthread_cond_destroy(&m_requestThread.cond);

    pthread_join(m_resultThread.thread, NULL);
    pthread_cond_signal(&m_resultThread.cond);
    pthread_mutex_unlock(&m_resultThread.mutex);
    pthread_mutex_destroy(&m_resultThread.mutex);
    pthread_cond_destroy(&m_resultThread.cond);

    if (m_pDevice != NULL)
    {
        m_pDevice->common.close(&m_pDevice->common);

        if (m_pVideoFilehandle > 0)
        {
            fclose(m_pVideoFilehandle);
        }

        m_pDevice = NULL;
    }
    for (uint32_t i = 0; i < StreamTypeMax; i++)
    {
        if (m_pBufferManager[i] != NULL)
        {
            delete m_pBufferManager[i];
            m_pBufferManager[i] = NULL;
        }
    }

    if (m_pTOFInterface)
    {
        TOFDestroyInterface(m_pTOFInterface);
        m_pTOFInterface = NULL;
    }

}

// -----------------------------------------------------------------------------------------------------------------------------
// Function that will process one capture result sent from the camera module. Remember this function is operating in the camera
// module thread context. So we do the bare minimum work that we need to do and return control back to the camera module. The
// bare minimum we do is to dispatch the work to another worker thread who consumes the image buffers passed to it from here.
// Our result worker thread is "ThreadPostProcessResult(..)"
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgrTof::ProcessOneCaptureResult(const camera3_capture_result* pHalResult)
{

    if (pHalResult->num_output_buffers > 0)
    {
        CaptureResultFrameData* pCaptureResultData = new CaptureResultFrameData;

        // The camera frames is from UKNOWN_SOURCE timestamp. Therefore use CLOCK_REALTIME for camera frames timestamp in case
        // we want to sync camera frame timestamp with some other data also in CLOCK_REALTIME.
        int64_t imageTimestampNsecs;
        static struct timespec temp;
        clock_gettime(CLOCK_REALTIME, &temp);
        imageTimestampNsecs = (temp.tv_sec*1e9) + temp.tv_nsec;

        memset(pCaptureResultData, 0, sizeof(CaptureResultFrameData));

        pCaptureResultData->frameNumber    = pHalResult->frame_number;
        pCaptureResultData->timestampNsecs = imageTimestampNsecs;

        // Go through all the output buffers received. It could be preview only, video only, or preview + video
        for (uint32_t i = 0; i < pHalResult->num_output_buffers; i++)
        {
            buffer_handle_t* pImageBuffer;

            if (pHalResult->output_buffers[i].stream == &m_streams[StreamTypePreview])
            {
                pImageBuffer = pHalResult->output_buffers[i].buffer;
                pCaptureResultData->pPreviewBufferInfo = m_pBufferManager[StreamTypePreview]->GetBufferInfo(pImageBuffer);
                m_pBufferManager[StreamTypePreview]->PutBuffer(pImageBuffer); // This queues up the buffer for recycling
            }
            else if (pHalResult->output_buffers[i].stream == &m_streams[StreamTypeVideo])
            {
                pImageBuffer = pHalResult->output_buffers[i].buffer;
                pCaptureResultData->pVideoBufferInfo = m_pBufferManager[StreamTypeVideo]->GetBufferInfo(pImageBuffer);
                m_pBufferManager[StreamTypeVideo]->PutBuffer(pImageBuffer); // This queues up the buffer for recycling
            }
        }

        // Mutex is required for msgQueue access from here and from within the thread wherein it will be de-queued
        pthread_mutex_lock(&m_resultThread.mutex);
        // Queue up work for the result thread "ThreadPostProcessResult"
        m_resultThread.msgQueue.push_back((void*)pCaptureResultData);
        pthread_cond_signal(&m_resultThread.cond);
        pthread_mutex_unlock(&m_resultThread.mutex);
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// PerCameraMgrTof::CameraModuleCaptureResult(..) is the entry callback that is registered with the camera module to be called when
// the camera module has frame result available to be processed by this application. We do not want to do much processing in
// that function since it is being called in the context of the camera module. So we do the bare minimum processing and leave
// the remaining process upto this function. PerCameraMgrTof::CameraModuleCaptureResult(..) just pushes a message in a queue that
// is monitored by this thread function. This function goes through the message queue and processes all the messages in it. The
// messages are nothing but camera images that this application has received.
// -----------------------------------------------------------------------------------------------------------------------------
void* ThreadPostProcessResultTof(void* pData)
{
    // Set thread priority
    pid_t tid = syscall(SYS_gettid);
    int which = PRIO_PROCESS;
    int nice  = -10;

    setpriority(which, tid, nice);

    ThreadData*             pThreadData        = (ThreadData*)pData;
    void*                   pTOFInterface      = ((PerCameraMgrTof*)pThreadData->pCameraMgr)->GetTOFInterface();
    int                     frame_number       = 0;
    uint8_t*                pRaw8bit           = NULL;

    // The condition of the while loop is such that this thread will not terminate till it receives the last expected image
    // frame from the camera module
    while (!pThreadData->EStop && !pThreadData->stop &&
           pThreadData->lastResultFrameNumber != frame_number)
    {
        pthread_mutex_lock(&pThreadData->mutex);

        if (pThreadData->msgQueue.empty())
        {
            struct timespec tv;
            clock_gettime(CLOCK_MONOTONIC, &tv);
            tv.tv_sec += 1;

            // Go to a temporary small sleep waiting for the result frame to arrive
            if(pthread_cond_timedwait(&pThreadData->cond, &pThreadData->mutex, &tv) != 0)
            {
                pthread_mutex_unlock(&pThreadData->mutex);
                continue;
            }
        }

        // Coming here means we have a result frame to process

        CaptureResultFrameData* pCaptureResultData = (CaptureResultFrameData*)pThreadData->msgQueue.front();

        pThreadData->msgQueue.pop_front();
        pthread_mutex_unlock(&pThreadData->mutex);

        // Handle dumping preview frames to files
        if (pCaptureResultData->pPreviewBufferInfo != NULL && pTOFInterface != NULL)
        {
            TOFProcessRAW16(pTOFInterface,
                            (uint16_t*)pCaptureResultData->pPreviewBufferInfo->vaddr,
                            pCaptureResultData->timestampNsecs);

        }

        delete pCaptureResultData;

        frame_number = pCaptureResultData->frameNumber;
    }

    if (pRaw8bit != NULL)
    {
        free(pRaw8bit);
    }

    printf("------ Last result frame: %d\n", frame_number);
    fflush(stdout);

    return NULL;
}

static int frame_number = 0;
// -----------------------------------------------------------------------------------------------------------------------------
// The TOF library calls this function when it receives data from the Royale PMD libs
// -----------------------------------------------------------------------------------------------------------------------------
bool PerCameraMgrTof::RoyaleDataDone(const void*                 pData,
                                     uint32_t                    size,
                                     int64_t                     timestamp,
                                     modalai::RoyaleListenerType dataType)
{

    const royale::DepthData* pDepthData               = static_cast<const royale::DepthData *> (pData);
    const royale::Vector<royale::DepthPoint>& pointIn = pDepthData->points;
    int numPoints = (int)pointIn.size();
    const ApiInterfaceData* pApiData                  = GetApiInterfaceData();

    uint64_t timediff_ns =  _time_realtime_ns() - _time_monotonic_ns();

    camera_image_metadata_t IRMeta, DepthMeta, ConfMeta, NoiseMeta;
    point_cloud_metadata_t PCMeta;

    IRMeta.timestamp_ns = (pDepthData->timeStamp.count() )-timediff_ns;
    IRMeta.gain         = 0;
    IRMeta.exposure_ns  = 0;
    IRMeta.frame_id     = frame_number;
    IRMeta.width        = pDepthData->width;
    IRMeta.height       = pDepthData->height;

    DepthMeta = IRMeta;
    ConfMeta  = IRMeta;
    NoiseMeta = IRMeta;

    IRMeta.stride         = IRMeta.width * sizeof(uint8_t);
    IRMeta.size_bytes     = IRMeta.stride * IRMeta.height;
    IRMeta.format         = IMAGE_FORMAT_RAW8;

    DepthMeta.stride      = DepthMeta.width * sizeof(float);
    DepthMeta.size_bytes  = DepthMeta.stride * DepthMeta.height;
    DepthMeta.format      = IMAGE_FORMAT_FLOAT32;

    ConfMeta.stride       = ConfMeta.width * sizeof(uint8_t);
    ConfMeta.size_bytes   = ConfMeta.stride * ConfMeta.height;
    ConfMeta.format       = IMAGE_FORMAT_RAW8;

    NoiseMeta.stride      = NoiseMeta.width * sizeof(float);
    NoiseMeta.size_bytes  = NoiseMeta.stride * NoiseMeta.height;
    NoiseMeta.format      = IMAGE_FORMAT_FLOAT32;

    PCMeta.timestamp_ns   = IRMeta.timestamp_ns;
    PCMeta.n_points       = numPoints;

    uint8_t    IRData     [numPoints];
    float      DepthData  [numPoints];
    uint8_t    ConfData   [numPoints];
    float      NoiseData  [numPoints];
    float      PointCloud [numPoints*3];
    tof_data_t FullData;

    FullData.magic_number = TOF_MAGIC_NUMBER;
    FullData.timestamp_ns = IRMeta.timestamp_ns;

    for (int i = 0; i < numPoints; i++)
    {
        royale::DepthPoint point = pointIn[i];

        IRData[i] = point.grayValue > 255 ? 255 : point.grayValue;

        DepthData[i] = point.z;

        ConfData[i] = point.depthConfidence;

        NoiseData[i] = point.noise;

        PointCloud[(i*3)]   = point.x;
        PointCloud[(i*3)+1] = point.y;
        PointCloud[(i*3)+2] = point.z;

        FullData.points     [i][0] = point.x;
        FullData.points     [i][1] = point.y;
        FullData.points     [i][2] = point.z;
        FullData.noises     [i]    = point.noise;
        FullData.grayValues [i]    = point.grayValue;
        FullData.confidences[i]    = point.depthConfidence;
    }

    // Ship the frames out of the camera server
    pApiData->sendCameraFrame(m_outputChannel + IR_CH_OFFSET,    IRMeta,             IRData);
    pApiData->sendCameraFrame(m_outputChannel + DEPTH_CH_OFFSET, DepthMeta,          DepthData);
    pApiData->sendCameraFrame(m_outputChannel + CONF_CH_OFFSET,  ConfMeta,           ConfData);
    pApiData->sendCameraFrame(m_outputChannel + NOISE_CH_OFFSET, NoiseMeta,          NoiseData);
    pApiData->sendPointCloud (m_outputChannel + PC_CH_OFFSET,    PCMeta,             PointCloud);
    pApiData->sendGeneric    (m_outputChannel + FULL_CH_OFFSET,  sizeof(tof_data_t), (const char *)(&FullData));

    frame_number++;
    return true;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Load the TOF sensor lens parameters from "irs10x0c_lens.cal"
// -----------------------------------------------------------------------------------------------------------------------------
bool PerCameraMgrTof::LoadLensParamsFromFile()
{
    const char* LensParamsFilePath = "/data/misc/camera/irs10x0c_lens.cal";
    const char* RequiredVersion    = "VERSION:1.0:VERSION";

    std::vector<float> v_float;
    std::ifstream ifs;
    std::string line;
    int  maxTries = 10;
    bool status   = false;

    while (maxTries-- > 0)
    {
        ifs.open(LensParamsFilePath, std::ifstream::in);

        if( ifs.is_open() )
        {
            maxTries = 0;
        }
        else
        {
            usleep(1000000);
        }
    }

    if (!ifs.is_open())
    {
        VOXL_LOG_ERROR("Error: cannot open lens parameters file %s.", LensParamsFilePath );
    }
    else
    {
        status = std::getline(ifs, line);

        if (!status || strncmp(line.c_str(), RequiredVersion, strlen(RequiredVersion)) != 0)
        {
            VOXL_LOG_ERROR("Lens file Error: the first line of %s must be %s", LensParamsFilePath, RequiredVersion);
        }
        else
        {
            while(std::getline(ifs, line))
            {
                if(line.length() > 0 && line[0] != '#')
                {
                    float num;
                    std::stringstream linestream(line);

                    while (linestream >> num)
                    {
                        v_float.push_back(num);
                    }
                }
            }
        }

        ifs.close();
    }

    status = (v_float.size() >= 8);

    if(status)
    {


        cJSON* head = cJSON_CreateObject();
        VOXL_LOG_INFO("Loading lens parameters from %s.", LensParamsFilePath);


        m_lensParameters.principalPoint       = royale::Pair<float, float>(v_float[0], v_float[1]);
        cJSON_AddNumberToObject(head, "cx", v_float[0]);
        cJSON_AddNumberToObject(head, "cy", v_float[1]);

        m_lensParameters.focalLength          = royale::Pair<float, float>(v_float[2], v_float[3]);
        cJSON_AddNumberToObject(head, "fx", v_float[2]);
        cJSON_AddNumberToObject(head, "fy", v_float[3]);

        m_lensParameters.distortionTangential = royale::Pair<float, float>(v_float[4], v_float[5]);
        cJSON_AddNumberToObject(head, "tan0", v_float[4]);
        cJSON_AddNumberToObject(head, "tan1", v_float[5]);

        char name[5] = {'r','a','d',0,0};
        for (unsigned int i = 6; i < v_float.size(); i++)
        {
            m_lensParameters.distortionRadial.push_back(v_float[i]);
            name[3] = '0' - 6 + i;
            cJSON_AddNumberToObject(head, name, v_float[i]);

        }

        int baseChannel = g_outputChannels[CAMTYPE_TOF];
        for(int i = 0; i < NUM_TOF_CHANNELS; i++){

            pipe_server_set_info_string(baseChannel+i, cJSON_Print(head));

        }
        cJSON_Delete(head);
    }

     return status;
}
