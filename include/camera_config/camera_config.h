/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef CAMERA_CONFIG_H
#define CAMERA_CONFIG_H

#include <map>
#include <iostream>
#include <stdint.h>
#include <string>
#include "common_defs.h"
#include "cJSON.h"

#define JsonPortStrings(x) (x == 0 ? JsonPortJ2String : (x == 1 ? JsonPortJ3String : JsonPortJ4String))

//------------------------------------------------------------------------------------------------------------------------------
// Class that reads from the config file. Potential support for different config file formats should be done here to keep it
// isolated from the rest of the code
//
// @todo Need to handle multiple cameras of the same type
//------------------------------------------------------------------------------------------------------------------------------
class ConfigFile
{
public:
    // Read and parse the config file given in the command line argument
    static Status Read(const char*     pConfigFileName,     ///< Config filename
                       uint32_t*       pNumCameras,         ///< Returned number of cameras detected in the config file
                       PerCameraInfo** ppPerCameraInfo);    ///< Returned camera info for each camera in the config file

    // Read and parse the config file given in the command line argument
    static Status Write(const char*     pConfigFileName,       ///< Config filename
                        int             pNumCameras,           ///< Passed number of cameras
                        PerCameraInfo   pPerCameraInfo[]);     ///< Passed camera info for each camera in the config file

    static void PrintCameraInfo(PerCameraInfo* pCameraInfo);    ///< Camera info of all cameras

private:
    // Disable instantiation of this class
    ConfigFile();
    ~ConfigFile();

    // Parse the JSON entries from the linked list represented by pJsonParent
    static Status GetCameraInfo(cJSON*          pJsonParent,       ///< Main Json linked list
                                const char*     pPortName,         ///< Port on which the camera is connected
                                PerCameraInfo*  pPerCameraInfo);   ///< Camera info from the config file
    static bool         IsConfigFileVersionSupported(cJSON* pJsonParent);
    static CameraType   GetCameraType(cJSON* pCameraInfo);
    static CameraAPI    GetCameraApi(cJSON* pCameraInfo);
    static CameraAEAlgo GetCameraAEAlgo(cJSON* pAutoExposureInfo);
    static int32_t      GetV01CameraFormat(CameraMode mode, cJSON* pCameraInfo);
    static void         GetV01Streams(cJSON*          pJsonCamPort,
                                      PerCameraInfo*  pPerCameraInfo);    ///< Returned camera info for each camera
    static int          GetIntValue(cJSON*      pCameraInfo,    ///< Pointer to the entire camera info element
                                    const char* pString);       ///< Get the integer value associated with this string element

    // These are the main element strings in the JSON camera config file - so to speak these are variable names in the config
    // file. Each of these variables have values associated with them and the values could be integer, int64, strings etc
    static constexpr char*  DoesNotExistString          = (char*)"doesnotexist";             ///< Does not exist string
    static constexpr char*  JsonPortJ2String            = (char*)"port_J2";                  ///< Camera on port J2
    static constexpr char*  JsonPortJ3String            = (char*)"port_J3";                  ///< Camera on port J3
    static constexpr char*  JsonPortJ4String            = (char*)"port_J4";                  ///< Camera on port J4
    static constexpr char*  JsonVersionString           = (char*)"version";                  ///< Config file version
    static constexpr char*  JsonTypeString              = (char*)"type";                     ///< Camera type
    static constexpr char*  JsonNameString              = (char*)"name";                     ///< Camera name
    static constexpr char*  JsonApiString               = (char*)"api";                      ///< Camera Api
    static constexpr char*  JsonPreviewString           = (char*)"preview";                  ///< Preview
    static constexpr char*  JsonVideoString             = (char*)"video";                    ///< Preview
    static constexpr char*  JsonSnapshotString          = (char*)"snapshot";                 ///< Preview
    static constexpr char*  JsonWidthString             = (char*)"width";                    ///< Frame width
    static constexpr char*  JsonHeightString            = (char*)"height";                   ///< Frame height
    static constexpr char*  JsonFormatString            = (char*)"format";                   ///< Frame format
    static constexpr char*  JsonRotationString          = (char*)"rotation";                 ///< Rotation angle
    static constexpr char*  JsonFpsString               = (char*)"frame_rate";               ///< Fps
    static constexpr char*  JsonTofModeString           = (char*)"tof_mode";                 ///< TOF Mode
    static constexpr char*  JsonAutoExposureString      = (char*)"auto_exposure_mode";       ///< Autoexposure mode
    static constexpr char*  JsonExposureNsString        = (char*)"manual_exposure_nsecs";    ///< Manual exposure in nanoseconds
    static constexpr char*  JsonGainString              = (char*)"manual_gain";              ///< Manual gain (ISO)
    static constexpr char*  JsonCPAFiltSizeString       = (char*)"mv_cpa_filter_size";       ///< MV CPA Filter Size
    static constexpr char*  JsonCPAExpCostString        = (char*)"mv_cpa_exposure_cost";     ///< MV CPA Exp cost
    static constexpr char*  JsonCPAGainCostString       = (char*)"mv_cpa_gain_cost";         ///< MV CPA Gain cost
    static constexpr char*  JsonCPAHistogramString      = (char*)"mv_cpa_en_histogram";      ///< MV CPA enable histogram
    static constexpr char*  JsonOverrideIdString        = (char*)"override_id";              ///< Override id
    static constexpr char*  JsonEnabledString           = (char*)"enabled";                  ///< Is camera enabled
    static constexpr double SupportedVersion            = 0.2;                               ///< Supported config file version

    //v0.1 relics to keep around for backwards compatability
    static constexpr char*  JsonPWidthString       = (char*)"preview_width";            ///< Preview Frame width
    static constexpr char*  JsonPHeightString      = (char*)"preview_height";           ///< Preview Frame height
    static constexpr char*  JsonPFormatString      = (char*)"preview_format";           ///< Preview Frame format
    static constexpr char*  JsonVWidthString       = (char*)"video_width";              ///< Video Frame width
    static constexpr char*  JsonVHeightString      = (char*)"video_height";             ///< Video Frame height
    static constexpr char*  JsonVFormatString      = (char*)"video_format";             ///< Video Frame format
    static constexpr char*  JsonSWidthString       = (char*)"snapshot_width";           ///< Snapshot Frame width
    static constexpr char*  JsonSHeightString      = (char*)"snapshot_height";          ///< Snapshot Frame height
    static constexpr char*  JsonSFormatString      = (char*)"snapshot_format";          ///< Snapshot Frame format
    static constexpr char*  JsonDisabledString     = (char*)"disabled";                 ///< Is camera disabled

    static std::map<std::string, int> m_sFmtMap[3];         ///< Preview format to enum mapping

    static double        m_sFileVersion;                        ///< Config file version
    static const int32_t MaxSupportedCameras = 32;              ///< Max number of cameras supported
    static PerCameraInfo m_sPerCameraInfo[MaxSupportedCameras]; ///< Per camera info
    static int32_t       m_sNumCameras;                         ///< Number of cameras
};

#endif // end #define CAMERA_CONFIG_H
