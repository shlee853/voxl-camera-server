#ifndef MV_CAC_H
#define MV_CAC_H

/***************************************************************************//**
@file
   mvCAC.h

@detailed
   Machine Vision,
   Camera Auto-Calibration (CAC)

@section Overview
   This module performs mono camera auto-calibration, which does not require
   a known pattern in front of the camera.  It calibrates the following
   parameters: camera intrinsic, lens distortion, camera-gyro orientation,
   camera-gyro time offset.

@section Limitations
   CAC may produce incorrect results if the following conditions are not met:
   - Exposure time must be shorter than 5ms.
   - Adjacent frames must have similar brightness level.  It is recommended
     to use constant exposure and gain, if AEC cannot meet this requirement.

@section Recommendations
   CAC converges in 900 frames if the following conditions are met:
   - Camera rotates around all 3 axis.
   - Median inter-frame camera rotation is at least 2 degrees.
   - 99% of inter-frame camera rotation is no more than 6 degrees.
   - At least 80% of the scene is textured, and at least 3 meters away.
   - Inter-frame camera translation is no more than 1 cm.
   - Restriction on camera translation can be relaxed in proportion to scene
     distance.  For example, if 80% of the scene is 30 meters away, CAC can
     tolerate inter-frame camera translation up to 10 cm.
   - CAC allows exposure variation, but overall gain (exposure + ISO) should 
     either stay constant or change slowly.  The following requirements must be 
     met:
     1. Interval in between still shots is small;
     2. Brightness of the same object does not change much between adjacent 
        frames; and
     3. Cannot mix flash and no-flash frames in a segment.

@internal
   Copyright 2017 Qualcomm Technologies, Inc.  All rights reserved.
   Confidential & Proprietary.
*******************************************************************************/


//==============================================================================
// Defines
//==============================================================================


//==============================================================================
// Includes
//==============================================================================

#include <mv.h>

//==============================================================================
// Declarations
//==============================================================================

#ifdef __cplusplus
extern "C"
{
#endif


   /************************************************************************//**
   @detailed
      Camera Auto-Calibration (CAC)
   ****************************************************************************/
   typedef struct mvCAC mvCAC;


   /************************************************************************//**
   @detailed
      Configuration parameters for initializing mvCAC.
   @param maxNumKeypoints
      Maximum number of key points used for tracking.
   @param tauGyroCamera
      Initial value for time offset from camera to gyroscope, in microseconds.
   @param tauRollingShutterSkew
      Initial value for offset of rolling shutter skew, in microseconds.
   @param principalPointErrorStddev
      Standard deviation of the error between initial principal point and the
      ground truth, unit-less (normalized by image size).
   @param focalRatioErrorStddev
      Standard deviation of the error between initial focal ratio and the
      ground truth, unit-less (focal ratio = focal length / image width).
   @param distortionErrorStddev
      Standard deviation of the error between initial lens distortion
      parameters and the ground truth.
   @param rbcErrorStddev
      Standard deviation of the error between initial gyro-camera orientation
      and the ground truth.  Measured for rbc in axis-angle representation.
   @param tauGCErrorStddev
      Standard deviation of the error between initial gyro-camera time offset
      and the ground truth, in microseconds.
   @param tauRSSErrorStddev
      Standard deviation of the error between initial offset of rolling shutter
      skew and the ground truth, in microseconds.
   ****************************************************************************/
   struct mvCACConfiguration
   {
      float32_t maxNumKeypoints = 200;
      float32_t tauGyroCamera = 0;
      float32_t tauRollingShutterSkew = 0;
      float32_t principalPointErrorStddev = 0.1 / 3.0;
      float32_t focalRatioErrorStddev = 0.3 / 3.0;
      float32_t distortionErrorStddev = 0.33 / 3.0;
      float32_t rbcErrorStddev = 3.14 / 3.0;
      float32_t tauGCErrorStddev = 3e4 / 3.0;
      float32_t tauRSSErrorStddev = 1e4 / 3.0;
   };


   /************************************************************************//**
   @detailed
      CAC status.
   @param reprojectionError
      Re-projection error in pixels.
   @param inlierRatio
      Inlier ratio.
   ****************************************************************************/
   struct mvCACStatus
   {
      float32_t reprojectionError;
      float32_t inlierRatio;
   };


   /************************************************************************//**
   @detailed
      Initialize Camera Auto-Calibration (CAC) object.
   @param pCamCfg
      Initial values for camera calibration parameters.
      memoryStride and uvOffset are ignored.
   @param mask
      Mask of good camera pixels.  Its size is the same as camera frame size.
      0 means the corresponding camera pixel should be ignored by CAC.
      >0 means the corresponding camera pixel should be processed by CAC.
      If mask == nullptr, CAC will process all camera pixels, as if all mask
      pixels > 0.
   @param maskStride
      Stride of mask in bytes. Ignored when mask == nullptr.
   @param pRbc
      Initial value for rotation from camera coordinate frame to body
      coordinate frame. It is assumed that the gyroscope frame is the same as
      the body frame.
   @param pCACCfg
      CAC configuration parameters.
   @return
      Pointer to CAC object; returns NULL if failed.
   ****************************************************************************/
   MV_API mvCAC* mvCAC_Initialize( const mvCameraConfiguration* pCamCfg,
                                   const uint8_t* mask, uint32_t maskStride,
                                   const mvPose3DR* pRbc,
                                   const mvCACConfiguration* pCACCfg );


   /************************************************************************//**
   @detailed
      Deinitialize Camera Auto-Calibration (CAC) object.
   @param pmObj
      Pointer to CAC object.
   ****************************************************************************/
   void MV_API mvCAC_Deinitialize( mvCAC* pObj );


   /************************************************************************//**
   @detailed
      Add gyro measurement
   @details
      Gyro measurements must be added in chronological order.  All measurements
      received before the end of the frame must be added before either
      mvCAC_AddFrame or mvCAC_AddTrackedPoints is called.
   @param pObj
      Pointer to CAC object.
   @param timestamp
      Timestamp of the gyro measurement, in microseconds.
   @param x
      Gyro measurement for X axis, in rad/s.
   @param y
      Gyro measurement for Y axis, in rad/s.
   @param z
      Gyro measurement for Z axis, in rad/s.
   ****************************************************************************/
   void MV_API mvCAC_AddGyro( mvCAC* pObj, int64_t timestamp,
                              float64_t x, float64_t y, float64_t z );


   /************************************************************************//**
   @detailed
      Add camera frame.
   @details
      This function performs feature tracking internally.  Call
      mvCAC_AddTrackedPoints instead for external tracking.
   @param pObj
      Pointer to CAC object.
   @param timestamp
      Timestamp of the first row at the center of exposure, in microseconds.
   @param rollingShutterSkew
      The duration between the start of first row exposure and the start of
      last row exposure, in microseconds.
   @param pixels
      Pointer to the pixels of luma channel.
   @param stride
      Stride of the luma channel in bytes.
   ****************************************************************************/
   void MV_API mvCAC_AddFrame( mvCAC* pObj, int64_t timestamp,
                               int64_t rollingShutterSkew,
                               const uint8_t* pixels, uint32_t stride );


   /************************************************************************//**
   @detailed
      Add tracked points in a camera frame.
   @details
      This function allows the caller to do its own feature tracking.  If the
      caller doesn't have one, call mvCAC_AddFrame instead.  The feature 
      tracker should have tracking error less than half of a pixel, and less 
      than 10% outliers.
   @param pObj
      Pointer to CAC object.
   @param timestamp
      Timestamp of the first row at the center of exposure, in microseconds.
   @param rollingShutterSkew
      The duration between the start of first row exposure and the start of
      last row exposure, in microseconds.
   @param pts1
      Tracked 2D points in the previous frame. X, Y coordinates are stored
      as @f$ (x_k, y_k) = (pts1[k*2], pts1[k*2+1]). k = 0 .. numPts-1 @f$
   @param pts2
      Tracked 2D points in the current frame.
   @param numPts
      Number of tracked points.
   ****************************************************************************/
   void MV_API mvCAC_AddTrackedPoints( mvCAC* pObj, int64_t timestamp,
                                       int64_t rollingShutterSkew,
                                       const float32_t* pts1,
                                       const float32_t* pts2, uint32_t numPts );


   /************************************************************************//**
   @detailed
      Get the calibration result.
   @param pObj
      Pointer to CAC object.
   @param pCfg
      Calibrated intrinsic parameters.  Set to nullptr if not needed.
      The distortion model is always fisheye (model 10).
   @param pRbc
      Calibrated Rbc.  Set to nullptr if not needed.
   @param tauGyroCamera
      Calibrated time offset from camera to gyroscope, in microseconds.
      Set to nullptr if not needed.
   @param tauRollingShutterSkew
      Calibrated offset of rolling shutter skew, in microseconds.
      Set to nullptr if not needed.
   @param td
      Calibrated time offset between camera and attitude, in microseconds.
      Set to nullptr if not needed.
   @param pStatus
      CAC status.  Set to nullptr if not needed.
   @return
      Tracking state.  No calibration result is returned if state < 0.
   ****************************************************************************/
   MV_TRACKING_STATE MV_API
   mvCAC_GetCalibration( mvCAC* pObj,
                         mvCameraConfiguration* pCfg,
                         mvPose3DR* pRbc,
                         float64_t* tauGyroCamera,
                         float64_t* tauRollingShutterSkew,
                         mvCACStatus* pStatus );


   /************************************************************************//**
   @detailed
      Convert fisheye model to polynomial model.
   @details
      See mv.h for more details on distortion model.
   @param pCfg
      Pointer to the camera parameters to be converted and updated.  The input
      distortion model must be 10, otherwise no conversion will be performed.
   @param model
      Desired distortion model, which can be either 4, 5, or 8.  No conversion
      will be performed if the specified model is not allowed.
   @return
      RMSE between output and input model, in pixels.  Returns 0 if conversion
      is not performed.
   ****************************************************************************/
   float64_t MV_API mvCAC_FisheyeToPolynomial( mvCameraConfiguration* pCfg,
                                               int32_t model );


   /************************************************************************//**
   @detailed
      Score the scene for textureness.
   @param pixels
      Pointer to the pixels of luma channel.
   @param width
      Image width.
   @param height
      Image height.
   @param stride
      Image stride in bytes.
   @return
      Score between 0 and 1.  Higher score means more texture in the scene.
   ****************************************************************************/
   float32_t MV_API mvCAC_ScoreSceneTexture( const uint8_t* pixels,
                                             uint32_t width, uint32_t height,
                                             uint32_t stride );


#ifdef __cplusplus
}
#endif

#endif
