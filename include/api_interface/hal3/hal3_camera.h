/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef VOXL_HAL3_CAMERA
#define VOXL_HAL3_CAMERA

#include <camera/CameraMetadata.h>
#include <hardware/camera3.h>
#include <list>
#include <string>
#include "api_interface_generic.h"
#include "common_defs.h"
#include "voxl_camera_server.h"
#include <mutex>
#include <condition_variable>
#include <royale/LensParameters.hpp>
#include "TOFInterface.h"

#define TOF_RAW_FRAME_MODE5_WIDTH  224
#define TOF_RAW_FRAME_MODE5_HEIGHT 865
#define TOF_RAW_FRAME_MODE9_WIDTH  224
#define TOF_RAW_FRAME_MODE9_HEIGHT 1557
#define TOF_RAW_FRAME_MODE11_WIDTH  224
#define TOF_RAW_FRAME_MODE11_HEIGHT 1903

//HAL3 will lag the framerate if we attempt autoexposure any more frequently than this
#define MVCPA_NUM_SKIPPED_FRAMES 4

using namespace std;

// Forward Declaration
class BufferManager;
class PerCameraMgr;
class VideoEncoder;
class ImageRotation;
class StereoImageSplit;

struct ExpGainInterface;
struct VideoEncoderConfig;

// -----------------------------------------------------------------------------------------------------------------------------
// Different camera modes
// -----------------------------------------------------------------------------------------------------------------------------
static const uint32_t CameraModePreview  = (1 << CAMMODE_PREVIEW);      ///< Preview mode
static const uint32_t CameraModeVideo    = (1 << CAMMODE_VIDEO);        ///< Video mode
static const uint32_t CameraModeSnapshot = (1 << CAMMODE_SNAPSHOT);     ///< Snapshot mode


// -----------------------------------------------------------------------------------------------------------------------------
// Thread Data for camera request and result thread
// -----------------------------------------------------------------------------------------------------------------------------
typedef struct ThreadData
{
    pthread_t         thread;                   ///< Thread handle
    pthread_mutex_t   mutex;                    ///< Mutex for list access
    pthread_cond_t    cond;                     ///< Condition variable for wake up
    std::list<void*>  msgQueue;                 ///< Message queue
    PerCameraMgr*     pCameraMgr;               ///< Pointer to the per camera mgr class
    camera3_device_t* pDevice;                  ///< Camera device that the thread is communicating with
    void*             pPrivate;                 ///< Any private information if need be
    volatile bool     stop;                     ///< Indication for the thread to terminate
    volatile bool     EStop;                    ///< Emergency Stop, terminate without waiting
    volatile int      lastResultFrameNumber;    ///< Last frame the capture result thread should wait for before terminating
} ThreadData;

// -----------------------------------------------------------------------------------------------------------------------------
// Structure containing all information that needs to be passed for each camera initialization to class PerCameraMgr
// -----------------------------------------------------------------------------------------------------------------------------
typedef struct PerCameraInitData
{
    const camera_module_t*  pCameraModule;      ///< Camera module
    const camera_info*      pHalCameraInfo;     ///< Camera info
    int                     cameraid;           ///< Camera id
    const ApiInterfaceData* pApiInterface;      ///< Callbacks into the camera server core
    const PerCameraInfo*    pPerCameraInfo;     ///< Per camera info
} PerCameraInitData;

//------------------------------------------------------------------------------------------------------------------------------
// Everything needed to handle a single camera
//------------------------------------------------------------------------------------------------------------------------------
class PerCameraMgr
{
public:
    PerCameraMgr();
    ~PerCameraMgr() {}

    // Return the stride of the frame
    CameraType GetCameraType() const
    {
        return m_cameraConfigInfo.type;
    }
    // Return the API interface pointer
    const ApiInterfaceData* GetApiInterfaceData() const
    {
        return m_pApiInterfaceData;
    }
    // Get number of frames skipped
    uint32_t GetNumSkippedFrames()
    {
        return MVCPA_NUM_SKIPPED_FRAMES;
    }
    // Set next exp/gain values
    void SetNextExpGain(int64_t exposure, int32_t gain)
    {
        m_nextExposure = exposure;
        m_nextGain     = gain;

        ///<@todo check if this is the right place
        m_expgainCondVar.notify_all();
    }
    // Get exposure/gain interface
    ExpGainInterface* GetExpGainInterface()
    {
        return m_pExpGainIntf;
    }
    // Return the VideoEncoder object
    VideoEncoder* GetVideoEncoder()
    {
        return m_pVideoEncoder;
    }
    // Return the ImageRotation object
    ImageRotation* GetImageRotation()
    {
        return m_pImageRotation;
    }
    // Return the ImageRotation object
    StereoImageSplit* GetStereoSplit()
    {
        return m_pStereoImageSplit;
    }
    // Get video buffer manager
    BufferManager* GetVideoBufferManager()
    {
        return m_pBufferManager[StreamTypeVideo];
    }
    // Get preview buffer manager
    BufferManager* GetPreviewBufferManager()
    {
        return m_pBufferManager[StreamTypePreview];
    }
    // Is preview mode enabled
    bool IsPreviewModeEnabled() const
    {
        return m_cameraConfigInfo.modeInfo[CAMMODE_PREVIEW].isEnabled;
    }
    // Is video mode enabled
    bool IsVideoModeEnabled() const
    {
        return m_cameraConfigInfo.modeInfo[CAMMODE_VIDEO].isEnabled;
    }
    // Is snapshot mode enabled
    bool IsSnapshotModeEnabled() const
    {
        return m_cameraConfigInfo.modeInfo[CAMMODE_SNAPSHOT].isEnabled;
    }
    // Get preview width
    int32_t GetPreviewWidth() const
    {
        return m_cameraConfigInfo.modeInfo[CAMMODE_PREVIEW].width;
    }
    // Get preview height
    int32_t GetPreviewHeight() const
    {
        return m_cameraConfigInfo.modeInfo[CAMMODE_PREVIEW].height;
    }
    // Get preview format
    int32_t GetPreviewFormat() const
    {
        return m_cameraConfigInfo.modeInfo[CAMMODE_PREVIEW].format;
    }
    // Get video width
    int32_t GetVideoWidth() const
    {
        return m_cameraConfigInfo.modeInfo[CAMMODE_VIDEO].width;
    }
    // Get video height
    int32_t GetVideoHeight() const
    {
        return m_cameraConfigInfo.modeInfo[CAMMODE_VIDEO].height;
    }
    // Get video format
    int32_t GetVideoFormat() const
    {
        return m_cameraConfigInfo.modeInfo[CAMMODE_VIDEO].format;
    }
    // Get snapshot width
    int32_t GetSnapshotWidth() const
    {
        return m_cameraConfigInfo.modeInfo[CAMMODE_SNAPSHOT].width;
    }
    // Get snapshot height
    int32_t GetSnapshotHeight() const
    {
        return m_cameraConfigInfo.modeInfo[CAMMODE_SNAPSHOT].height;
    }
    // Get snapshot format
    int32_t GetSnapshotFormat() const
    {
        return m_cameraConfigInfo.modeInfo[CAMMODE_SNAPSHOT].format;
    }
    // Get frame rate
    int32_t GetFrameRate() const
    {
        return m_cameraConfigInfo.fps;
    }
    // Get stereo left buffer
    void* StereoLeftBuffer() const
    {
        return m_pStereoLBuffer;
    }
    // Get stereo left buffer
    void* StereoRightBuffer() const
    {
        return m_pStereoRBuffer;
    }
    bool IsStopped(){
        return 
            m_requestThread.EStop ||
            m_resultThread.EStop;
    }
    void EStop(){

        m_requestThread.EStop = true;
        m_resultThread.EStop = true;

    }
    char *GetName(){
        return m_cameraConfigInfo.name;
    }
    int getNumClients(){
        return (int) m_numClients;
    }

    // Perform any one time initialization
    virtual int  Initialize(PerCameraInitData* pPerCameraInitData);
    // Start the camera so that it starts streaming frames
    virtual int  Start();
    // Stop the camera and stop sending any more requests to the camera module
    virtual void Stop();
    // Send one capture request to the camera module
    int  ProcessOneCaptureRequest(int frameNumber);
    // Process one capture result sent by the camera module
    virtual void ProcessOneCaptureResult(const camera3_capture_result* pHalResult);
    // The request thread calls this function to indicate it sent the last request to the camera module and will not send any
    // more requests
    void StoppedSendingRequest(int framenumber);
    void StopResultThread();

    void addClient();
    void removeClient();
    uint8_t                   m_outputChannel;

protected:

    // Camera module calls this function to pass on the capture frame result
    static void  CameraModuleCaptureResult(const camera3_callback_ops *cb,const camera3_capture_result *hal_result);
    // Camera module calls this function to notify us of any messages
    static void  CameraModuleNotify(const struct camera3_callback_ops *cb, const camera3_notify_msg_t *msg);
    // Check the static camera characteristics for a matching (w, h, format) combination
    bool         IsStreamConfigSupported(int width, int height, int format);
    // Call the cameram module and pass it the stream configuration
    int          ConfigureStreams();
    int          GetJpegBufferSize(uint32_t width, uint32_t height);
    // Allocate the stream buffers using the BufferManager
    int          AllocateStreamBuffers();
    // Call the camera module to get the default camera settings
    virtual void ConstructDefaultRequestSettings();

    ///<@todo Better way than hardcoding. These are pseudo-randomly selected values.
    static const uint32_t MaxPreviewBuffers  = 4;
    static const uint32_t MaxVideoBuffers    = 4;
    static const uint32_t MaxSnapshotBuffers = 2;
    static const uint32_t MaxFrameMetadata  = 32;
    ///<@todo Remove the hardcode to skip frames
    static const int      MinJpegBufferSize = (256 * 1024 + sizeof(camera3_jpeg_blob));

    // camera3_callback_ops is returned to us in every result callback. We piggy back any private information we may need at
    // the time of processing the frame result. When we register the callbacks with the camera module, we register the starting
    // address of this structure (which is camera3_callbacks_ops) but followed by our private information. When we receive a
    // pointer to this structure at the time of capture result, we typecast the incoming pointer to this structure type pointer
    // and access our private information
    struct Camera3Callbacks
    {
        camera3_callback_ops cameraCallbacks;
        void* pPrivate;
    };

    // Per frame value saved off from the metadata. It is indexed using framenumber.
    struct MetadataInfo
    {
        int64_t timestampNsecs; ///< Frame timestamp
        int64_t exposureNsecs;  ///< Frame exposure values indexed with frame number
        int     gain;           ///< Frame gain value
    };

    MetadataInfo              m_perFrameMeta[MaxFrameMetadata]; ///< Per frame metadata
    int32_t                   m_cameraId;                       ///< Camera id
    int32_t                   m_previewHalFmt;                  ///< HAL format to use for preview
    int32_t                   m_videoHalFmt;                    ///< HAL format to use for video
    int32_t                   m_snapshotHalFmt;                 ///< HAL format to use for snapshot
    Camera3Callbacks          m_cameraCallbacks;                ///< Camera callbacks
    const camera_module_t*    m_pCameraModule;                  ///< Camera module
    const camera_info*        m_pHalCameraInfo;                 ///< Camera info
    camera3_stream_t          m_streams[StreamTypeMax];         ///< Streams to be used for the camera request
    camera3_device_t*         m_pDevice;                        ///< HAL3 device
    android::CameraMetadata   m_requestMetadata;                ///< Per request metadata
    BufferManager*            m_pBufferManager[StreamTypeMax];  ///< Buffer manager per stream
    ThreadData                m_requestThread;                  ///< Request thread private data
    ThreadData                m_resultThread;                   ///< Result Thread private data
    const ApiInterfaceData*   m_pApiInterfaceData;              ///< Api interface data
    ExpGainInterface*         m_pExpGainIntf;                   ///< Exp/Gain interface
    int64_t                   m_currentExposure;                ///< Exposure target
    int32_t                   m_currentGain;                    ///< Gain target
    volatile int64_t          m_nextExposure;                   ///< Exposure target
    volatile int32_t          m_nextGain;                       ///< Gain target
    std::mutex                m_expgainCondMutex;               ///< Mutex to be used with the condition variable
    std::condition_variable   m_expgainCondVar;                 ///< Condition variable for wake up
    PerCameraInfo             m_cameraConfigInfo;               ///< Per camera config information
    const char*               m_pVideoFilename;                 ///< Video filename
    FILE*                     m_pVideoFilehandle;               ///< Video file handle
    VideoEncoder*             m_pVideoEncoder;                  ///< Video encoder for encoding the video frames
    VideoEncoderConfig*       m_pVideoEncoderConfig;            ///< Video encoder config
    ImageRotation*            m_pImageRotation;                 ///< Image rotation
    StereoImageSplit*         m_pStereoImageSplit;              ///< Stereo split rotation
    uint8_t*                  m_pStereoLBuffer;                 ///< Stereo left image
    uint8_t*                  m_pStereoRBuffer;                 ///< Stereo right image
    uint8_t                   m_numClients = 0;
};

class PerCameraMgrTof :public PerCameraMgr, public modalai::IRoyaleDataListener
{
public:

    PerCameraMgrTof();
    ~PerCameraMgrTof() {}

    void *GetTOFInterface(){
        return m_pTOFInterface;
    }

    // Perform any one time initialization
    int Initialize(PerCameraInitData* pPerCameraInitData) override;
    // Start the camera so that it starts streaming frames
    int Start() override;
    // Stop the camera and stop sending any more requests to the camera module
    void Stop() override;
    // Process one capture result sent by the camera module
    void ProcessOneCaptureResult(const camera3_capture_result* pHalResult) override;
    // Callback function for the TOF bridge to provide the post processed TOF data
    bool RoyaleDataDone(const void*             pData,
                        uint32_t                size,
                        int64_t                 timestamp,
                        modalai::RoyaleListenerType dataType);

protected:
    void ConstructDefaultRequestSettings() override;

private:
    void*                            m_pTOFInterface;                   ///< TOF interface to process the TOF camera raw data
    royale::LensParameters           m_lensParameters;                  ///< Lens parameters
    int                              m_laserScanLine;                   ///< Laser scan line
    int                              m_scanWidthDegrees;                ///< Scan witdh degrees

    bool LoadLensParamsFromFile();

};

//------------------------------------------------------------------------------------------------------------------------------
// Main interface class to manage all the cameras
//------------------------------------------------------------------------------------------------------------------------------
class CameraHAL3 : public ApiInterface
{
public:
    CameraHAL3();
    virtual ~CameraHAL3() { }
    // Perform any one time initialization
    Status         Initialize(ApiInterfaceData* pExtIntfData);
    virtual Status Start(const PerCameraInfo* pPerCameraInfo);
    virtual void   Destroy() { }

    void Stop(){
        if (m_pPerCameraMgr != NULL)
        {
            m_pPerCameraMgr->Stop();
            m_pPerCameraMgr = NULL;
        }
    }
    void EStop(){
        if (m_pPerCameraMgr != NULL)
        {
            m_pPerCameraMgr->EStop();
        }
    }

    void addClient(){
        if (m_pPerCameraMgr != NULL)
        {
            m_pPerCameraMgr->addClient();
        }
    }
    void removeClient(){
        if (m_pPerCameraMgr != NULL)
        {
            m_pPerCameraMgr->removeClient();
        }
    }
    int getNumClients(){
        if (m_pPerCameraMgr != NULL)
        {
            return m_pPerCameraMgr->getNumClients();
        }
        return 0;
    }
private:
    // Callback to indicate device status change
    static void CameraDeviceStatusChange(const struct camera_module_callbacks* callbacks, int camera_id, int new_status)
    {
    }
    // Callback to indicate torch mode status change
    static void TorchModeStatusChange(const struct camera_module_callbacks* callbacks, const char* camera_id, int new_status)
    {
    }

    // Load the camera module to start communicating with it
    int LoadCameraModule();

    static const uint32_t   MaxCameras = 4;                 ///< Max cameras
    static int32_t          m_sNumCameras;                  ///< Number of cameras detected
    static int32_t          m_sCamIdTOF;                    ///< ToF camera id
    static int32_t          m_sCamIdHiRes;                  ///< HiRes camera id
    static int32_t          m_sCamIdStereo;                 ///< Stereo camera id
    static int32_t          m_sCamIdTracking;               ///< Tracking camera id
    static int32_t          m_sIsInitialized;               ///< Is HAL3 interface initialized
    static camera_info      m_sCameraInfo[MaxCameras];      ///< Per camera info
    static camera_module_t* m_spCameraModule;               ///< Camera module

    camera_module_callbacks m_moduleCallbacks;              ///< Camera module callbacks
    PerCameraMgr*           m_pPerCameraMgr;                ///< Each instance manages one camera
    ApiInterfaceData        m_apiInterface;                 ///< Api interface
};

#endif // VOXL_HAL3_CAMERA
