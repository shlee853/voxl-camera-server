/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef VOXL_CAMERA_SERVER_PIPE_H
#define VOXL_CAMERA_SERVER_PIPE_H

#include <stdint.h>
#include <modal_pipe.h>
#include "common_defs.h"
#include "external_interface.h"

//------------------------------------------------------------------------------------------------------------------------------
// Class that uses the named pipe interface to send the camera frame data
//------------------------------------------------------------------------------------------------------------------------------
class NamedPipe : public ExternalInterface
{
public:
    // Create a singleton instance of this class
    static ExternalInterface* Create();
    // Destroy the instance
    virtual void Destroy();
    // Broadcast the camera frame to all clients
    Status BroadcastFrame(int channel, camera_image_metadata_t meta, char* pFrameData);
    // Broadcast the camera frame to all clients
    Status BroadcastPointCloud(int channel, point_cloud_metadata_t meta, float* pFrameData);
    // Broadcast generic data
    Status BroadcastGeneric(int channel, const char *data, int bytes);

private:
    // Disable direct instantiation of this class
    NamedPipe();
    virtual ~NamedPipe() {}

    // Do any one time initialization in this function
    virtual Status Initialize(int numCameras, const PerCameraInfo* pPerCameraInfo, ExternalInterfaceData* pExtIntfData);
    static  void   RequestPipeHandler(int channel, char* pClientName, int bytes, int clientid, void* pContext);
    static  void   DisconnectPipeHandler(int ch, int client_id, char* name, void* context);
    void           HandleClientRequest(int channel, char* pClientName, int bytes, int clientid);
    void           HandleClientDisconnect(int channel);
/*
    // Named pipe channel Ids for each camera
    static const int32_t MonoCameraChannelId   = 0;
    static const int32_t HiResCameraChannelId  = 1;
    static const int32_t TOFCameraChannelId    = 2;
    static const int32_t StereoCameraChannelId = 3;*/
    static const int32_t ControlPipeDisabled   = 0;
    static const int32_t ControlPipeEnabled    = 1;

    static NamedPipe*     m_spNamedPipe;                            ///< Single instance pointer of the class
};

#endif // #ifndef VOXL_CAMERA_SERVER_PIPE_H