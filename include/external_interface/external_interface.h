/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef EXTERNAL_INTERFACE_H
#define EXTERNAL_INTERFACE_H

#include <stdint.h>
#include <stddef.h>
#include "common_defs.h"

// Function prototypes for callbacks by the external interface into the camera server core
typedef void (*VoxlCamServerNewClientCb)(int CamId);
typedef void (*VoxlCamServerClientLeaveCb)(int CamId);

// -----------------------------------------------------------------------------------------------------------------------------
// External interface required data
// -----------------------------------------------------------------------------------------------------------------------------
struct ExternalInterfaceData
{
    VoxlCamServerNewClientCb    ClientJoined;
    VoxlCamServerClientLeaveCb  ClientLeft;
};

//------------------------------------------------------------------------------------------------------------------------------
// Abstract base class that provides an external interface for the core camera server to send camera frames and/or receive any
// information.
//
// The core camera server is agnostic of the internal details of this interface so that the interface can change without
// affecting anything in the core server.
//------------------------------------------------------------------------------------------------------------------------------
class ExternalInterface
{
public:
    // Create a singleton instance of this class
    static ExternalInterface* Create(int numCameras, const PerCameraInfo* pPerCameraInfo, ExternalInterfaceData* pIntfData);
    // Destroy the instance
    virtual void Destroy() = 0;
    // Initialize the instance
    virtual Status Initialize(int numCameras, const PerCameraInfo* pPerCameraInfo, ExternalInterfaceData* pIntfData) = 0;
    // Broadcast the camera frame to all clients
    virtual Status BroadcastFrame(int channel, camera_image_metadata_t meta, char* pFrameData) = 0;
    // Broadcast the camera frame to all clients
    virtual Status BroadcastPointCloud(int channel, point_cloud_metadata_t meta, float* pFrameData) = 0;
    // Broadcast the data to all clients
    virtual Status BroadcastGeneric(int channel, const char* data, int bytes) = 0;

protected:
    // Disable direct instantiation of this class
    ExternalInterface() {}
    virtual ~ExternalInterface() {}

    ExternalInterfaceData m_interfaceData;  ///< Any data pertaining to the server core
};

#endif // end #define EXTERNAL_INTERFACE_H
